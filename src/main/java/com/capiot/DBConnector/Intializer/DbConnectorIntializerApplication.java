package com.capiot.DBConnector.Intializer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbConnectorIntializerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbConnectorIntializerApplication.class, args);
	}

}
