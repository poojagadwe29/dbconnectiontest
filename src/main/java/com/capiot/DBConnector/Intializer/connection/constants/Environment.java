package com.capiot.DBConnector.Intializer.connection.constants;

public enum Environment {
        DEVELOPMENT,
        UAT,
        PRODUCTION;

        public static Environment toEnvironment(String environmentStr) {

            Environment environment;
            try {
                environment = Environment.valueOf(environmentStr.toUpperCase());
            } catch(Exception e) {
                throw new RuntimeException("Invalid Environment:" + environmentStr);
            }
            return environment;
        }


}
