package com.capiot.DBConnector.Intializer.connection.service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;

public interface ConnectionService
{
    JSONObject testConnection(JSONObject dbConnectionInfo);

    JSONArray getConnectionDetails(JSONObject dbConnectionInfo) throws SQLException;

}
