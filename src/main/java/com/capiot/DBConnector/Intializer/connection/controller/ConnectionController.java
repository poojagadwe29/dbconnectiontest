package com.capiot.DBConnector.Intializer.connection.controller;

import com.capiot.DBConnector.Intializer.connection.service.ConnectionService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.sql.SQLException;

@RestController
@CrossOrigin("*")
@RequestMapping("/connection")
public class ConnectionController
{
    @Autowired
    private ConnectionService connectionService;

    @PostMapping(value = "/v1/testConnection",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkConnection(@RequestBody String dbConnectionInfo)
    {
        System.out.println("Request:::"+ dbConnectionInfo.toString());

        JSONObject result = connectionService.testConnection(new JSONObject(dbConnectionInfo));
//        System.out.println("Response :: "+result.toString());
        return new ResponseEntity<>(result.toString(), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/getConnectionDetails",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getConnectionDetails(@RequestBody String dbConnectionInfo) throws SQLException {

        return new ResponseEntity<>(connectionService.getConnectionDetails(new JSONObject(dbConnectionInfo)).toString(), HttpStatus.OK);
    }






}
