package com.capiot.DBConnector.Intializer.connection.service.impl;

import com.capiot.DBConnector.Intializer.connection.service.ConnectionService;
import org.apache.commons.text.StringEscapeUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class ConnectionServiceImpl implements ConnectionService
{
    final  static Logger logger = Logger.getLogger(String.valueOf(ConnectionServiceImpl.class));

    @Autowired
    ApplicationArguments applicationArguments;


    public JSONObject testConnection(JSONObject dbConnectionInfo)
    {
        StandardPBEStringEncryptor encryptor = getStandardPBEStringEncryptor();
        logger.info("Connection testing......");
        boolean connectionFlag = false;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbConnectionInfo.getJSONObject("data").optJSONObject("databaseDetails").optString("url"),
                    dbConnectionInfo.getJSONObject("data").optJSONObject("credentials").optString("username"),
                    encryptor.decrypt(dbConnectionInfo.getJSONObject("data").optJSONObject("credentials").optString("password")));
            connectionFlag = conn != null ? true : false;
        } catch (SQLException e) {
            e.printStackTrace();
            connectionFlag = false;
            logger.info("Error while connecting database");
        }
        logger.info("Connection Status : "+connectionFlag);
        dbConnectionInfo.put("isConnected",connectionFlag);
        dbConnectionInfo.put("connectionObj",conn);
        dbConnectionInfo.getJSONObject("data").put("connectionStatus",connectionFlag==true?"DB Connected Successfully":"DB failed to connect");
        return dbConnectionInfo;
    }

    private StandardPBEStringEncryptor getStandardPBEStringEncryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(applicationArguments.getOptionValues("secretKey").get(0));
        encryptor.setAlgorithm("PBEWithMD5AndDES");
        return encryptor;
    }


    public JSONArray getConnectionDetails(JSONObject dbConnectionInfo) throws SQLException
    {
        JSONArray result = new JSONArray();
        List<String> schemas = new ArrayList<>();

        JSONObject testConnection = testConnection(dbConnectionInfo);
        boolean isConnected = testConnection.getBoolean("isConnected");
        if (isConnected)
        {
            Connection connection = (Connection) testConnection.get("connectionObj");
            if (connection!=null)
            {
                ResultSet resultSet = connection.getMetaData().getSchemas();
                while (resultSet.next()) {
                    schemas.add(resultSet.getString("TABLE_SCHEM"));
                }
                resultSet.close();

                extractDbDetails(result, schemas, connection);
            }

        }

        return result;
    }

    private void extractDbDetails(JSONArray result, List<String> schemas, Connection connection) throws SQLException
    {
        String[] types = { "TABLE" };
        JSONObject resultJson = null;
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet resultSet;
        for (String schema: schemas)
        {
            resultJson = new JSONObject();
            resultJson.put("schemaName",schema);

            resultSet = databaseMetaData.getTables("", schema, "%", types);

            JSONArray tables = new JSONArray();
            while(resultSet.next())
            {
                JSONObject tableDesc = new JSONObject();
                String tableName = resultSet.getString("TABLE_NAME");
                if (tableName!=null)
                {
                    tableDesc.put("tableName", tableName);

                    List<String> column = new ArrayList<>();
                    tableName = tableName.replaceAll("\\W+","");
//                    System.out.println("Table"+tableName);
                    ResultSet columns = databaseMetaData.getColumns(null, null, tableName, "%");
                    while (columns.next()) {
                        column.add(columns.getString("COLUMN_NAME"));
                    }
                    columns.close();
                    tableDesc.put("columnName", column);
                }
                tables.put(tableDesc);
                resultJson.put("tables", tables);
            }
            result.put(resultJson);
            resultSet.close();

        }
    }
}
